extends Node2D

@onready var bang_lines = $BangLines
@onready var bang_particles = $BangParticles
@onready var sound = $Sound
@onready var animation_player = $AnimationPlayer


func _ready():
	bang_lines.emitting = true
	bang_particles.emitting = true
	animation_player.play("explode")
	sound.play()


func hit(_d):
	pass


func _on_animation_player_animation_finished(_anim_name):
	queue_free()
