extends Node2D

@onready var bang_lines = $BangLines
@onready var bang_particles = $BangParticles
@onready var sound = $Sound


func _ready():
	bang_lines.emitting = true
	bang_particles.emitting = true
	sound.play()
	# Use a timer to make sure that we still delete even if muted.
	await get_tree().create_timer(sound.stream.get_length(), false).timeout
	queue_free()
