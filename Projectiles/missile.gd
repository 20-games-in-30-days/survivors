class_name Missile
extends RigidBody2D

@export var destroy_time := 5.0
@export var damage := 1
@export var thrust := 400.0  # Thrust force to apply for acceleration
@export var turn_speed := 1.0  # Adjust rotation speed
@export var max_damp := 5.0
@export var min_damp := 1.0
@export var damp_curve: Curve
@export var coast_time := 1.0

# Target node
var radar: Radar
var target: Node2D
var coast := true
var target_distance := 0.0


func _ready():
	destroy()
	await get_tree().create_timer(coast_time, false).timeout
	coast = false


func destroy():
	# Destroy the missile after a set amount of time, if applicable
	if destroy_time > 0:
		await get_tree().create_timer(destroy_time, false).timeout
		queue_free()


func _physics_process(delta):
	if coast:
		linear_damp = 0
		return
	if target and radar.has_target(target):
		# Calculate direction to target
		var dir_to_target = (target.global_position - global_position).normalized()
		var angle_to_target = atan2(dir_to_target.y, dir_to_target.x)
		var angle_diff = angle_to_target - rotation
		target_distance = global_position.distance_to(target.global_position)
		
		# Normalize the angle difference
		angle_diff = fmod(angle_diff + PI, 2 * PI) - PI
		
		# Increase friction if we are facing the wrong direction.
		var error = damp_curve.sample_baked(global_transform.x.dot(dir_to_target))
		linear_damp = max(min_damp, max_damp * error)
		
		# Apply angular velocity to rotate towards the target efficiently
		angular_velocity = angle_diff * turn_speed

		# Apply thrust in the facing direction
		var force = Vector2(thrust, 0).rotated(rotation)
		apply_central_impulse(force * delta)
	else:
		linear_damp = 0
		if radar.enemy_detected:
			target = radar.random_enemy
		else:
			queue_free()


func hit(_d):
	queue_free()


# Switch targets every now and then.
func _on_switch_timer_timeout():
	if target_distance > 512:
		target = radar.random_enemy
