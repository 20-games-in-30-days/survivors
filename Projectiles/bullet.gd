class_name Bullet
extends WrappedBody

## The time until the bullet is destroyed automatically. If negative, will never be destroyed.
@export var destroy_time := 1.0
@export var damage := 1


func _ready():
	# Destroy instead of coming back around.
	wrap_type = wrapType.DESTROY
	# Destroy the bullet after X seconds. We're not pooling until the profiler says that we have to.
	if destroy_time > 0:
		await get_tree().create_timer(destroy_time, false).timeout
		queue_free()


## Called when we collide with something.
func hit(_d):
	queue_free()
