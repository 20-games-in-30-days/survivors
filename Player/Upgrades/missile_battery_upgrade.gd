class_name MissileBatteryUpgrade extends Upgrade
@export var fire_delay := 0.01
@export var shots_per_cascade := 4
@export var cooldown_time := 4.0
@export var bullet_damage := 4
