class_name LaserTurretUpgrade extends Upgrade
## How long between shots (total, will divide evenly among the barrels)
@export var reload_time := 2.0
@export var bullet_damage := 2
@export_range(90, 900, 90, "radians_as_degrees") var rotation_speed := PI
@export_category("Gun Barrels Enable")
@export var barrel_a := false
@export var barrel_b := false
@export var barrel_c := false
@export var barrel_d := false
@export var barrel_e := false
