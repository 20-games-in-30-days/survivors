extends UpgradeCard

@export var upgrades: Array[LifeUpgrade]
@onready var life_counter = $".."
@onready var timer = $Timer
@onready var life_upgrade_card = $"../LifeUpgradeCard"


func _ready():
	super._ready()
	max_upgrade_level = upgrades.size() - 1


func stop():
	super.stop()
	timer.stop()


func upgradable():
	return super.upgradable() and life_upgrade_card.upgrade_active


func upgrade():
	if !upgrade_active:
		upgrade_active = true
	else:
		upgrade_level = min(upgrade_level + 1, max_upgrade_level)
	life_counter.regen_per_level = upgrades[upgrade_level].regens_per_upgrade
	life_counter.regen_all_lives()
	if upgrades[upgrade_level].regen_timer > 0:
		timer.start(upgrades[upgrade_level].regen_timer)


func _get_title() -> String:
	return upgrades[next_upgrade_level].label


func _get_details() -> String:
	return upgrades[next_upgrade_level].hint


func _on_timer_timeout():
	var temp = life_counter.regen_per_level
	life_counter.regen_per_level = 1
	life_counter.regen_life()
	life_counter.regen_per_level = temp
