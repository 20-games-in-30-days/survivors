class_name LaserRifleUpgrade extends Upgrade
## How long between shots (per barrel, is double the actual firing rate)
@export var reload_time := 2.0
@export var bullet_damage := 1
@export_category("Gun Barrels Enable")
@export var barrel_a := true
@export var barrel_b := true
@export var barrel_c := false
@export var barrel_d := false
@export var barrel_e := false
@export var barrel_f := false
