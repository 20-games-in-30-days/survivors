extends UpgradeCard

@export var upgrades: Array[LifeUpgrade]
@onready var life_counter = $".."


func _ready():
	super._ready()
	max_upgrade_level = upgrades.size() - 1


func upgrade():
	if !upgrade_active:
		logger.verbose("Extra Life Upgrade Card Activated!")
		upgrade_active = true
	else:
		upgrade_level = min(upgrade_level + 1, max_upgrade_level)
		logger.verbose("Extra Life Upgrade Card is now at level %s!" % upgrade_level)
	life_counter.unlock_life(upgrades[upgrade_level].life_count)


func _get_title() -> String:
	return upgrades[next_upgrade_level].label


func _get_details() -> String:
	return upgrades[next_upgrade_level].hint
