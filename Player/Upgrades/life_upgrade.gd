class_name LifeUpgrade extends Upgrade

## How many lives are currently unlocked.
@export var life_count := 0
@export var regens_per_upgrade := 0
@export var regen_timer := 0.0
