extends UpgradeCard

## Set the type to match your specific upgrade.
@export var upgrades: Array[Upgrade]


## Set the upgrade level based on the number of upgrades in the array.
func _ready():
	super._ready()
	max_upgrade_level = upgrades.size() - 1


## Sets the upgrade level, and applies the value.
func upgrade():
	super.upgrade()
	
	# TODO: Player.value = upgrades[upgrade_level].value


## Called when the game ends, reset our upgrades.
func stop():
	super.stop()
	
	# TODO: Player.value = upgrades[upgrade_level].value


## Get the title from the next upgrade.
func _get_title() -> String:
	return upgrades[next_upgrade_level].label


## Get the hint text from the next upgrade.
func _get_details() -> String:
	return upgrades[next_upgrade_level].hint
