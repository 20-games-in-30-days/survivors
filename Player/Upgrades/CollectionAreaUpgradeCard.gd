extends UpgradeCard

@export var upgrades: Array[CollectionAreaUpgrade]
@onready var attractor_area = $"../../MineralCollector/Attractor/CollisionShape2D"
var start_radius


## Set the upgrade level based on the number of upgrades in the array.
func _ready():
	super._ready()
	max_upgrade_level = upgrades.size() - 1
	start_radius = attractor_area.shape.radius


## Sets the upgrade level, and applies the value.
func upgrade():
	super.upgrade()
	attractor_area.shape.radius = start_radius * upgrades[upgrade_level].area_multiplier
	logger.debug("Collection area increased to %s" % attractor_area.shape.radius)


## Called when the game ends, reset our upgrades.
func stop():
	super.stop()
	attractor_area.shape.radius = start_radius


## Get the title from the next upgrade.
func _get_title() -> String:
	return upgrades[next_upgrade_level].label


## Get the hint text from the next upgrade.
func _get_details() -> String:
	return upgrades[next_upgrade_level].hint
