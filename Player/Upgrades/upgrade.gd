extends Resource
class_name Upgrade

@export_multiline var label := "Upgrade Title"
@export_multiline var hint := "This Upgrade Does Something"
