class_name RailgunUpgrade extends Upgrade
## How long between shots. Firing sequence is 5 seconds.
@export var cooldown_time := 15.0
@export var bullet_damage := 1028
@export var fire_animation := "fire1"
## How much the damage value is reduced by every time an asteroid is broken. Smaller numbers have higher persistence.
@export_category("Accents")
@export var accent1 := false
@export var accent2 := false
@export var accent3 := false
