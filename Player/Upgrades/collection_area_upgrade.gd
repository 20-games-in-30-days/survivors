extends Upgrade
class_name CollectionAreaUpgrade

## Base area modifier multiplier.
@export_range(1.0, 10.0, 0.25, "or_greater") var area_multiplier := 1.0
