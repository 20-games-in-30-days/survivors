class_name Radar extends Area2D

var tracked_enemies := []
var nearest_enemy = null:
	get:
		if !is_instance_valid(nearest_enemy):
			update_nearest_enemy()
		return nearest_enemy

var enemy_detected:
	get:
		return tracked_enemies.size() > 0

var random_enemy:
	get:
		if tracked_enemies.size() > 0:
			return tracked_enemies.pick_random()
		else:
			return null


func _ready():
	body_entered.connect(_on_body_entered)
	body_exited.connect(_on_body_exited)


func _process(_delta):
	update_nearest_enemy()


func _on_body_entered(body):
	tracked_enemies.append(body)


func _on_body_exited(body):
	if body in tracked_enemies:
		tracked_enemies.erase(body)


func has_target(target) -> bool:
	return is_instance_valid(target) and tracked_enemies.has(target)


func update_nearest_enemy():
	var nearest_distance = INF
	var nearest = null
	for enemy in tracked_enemies:
		var distance = global_position.distance_to(enemy.global_position)
		if distance < nearest_distance:
			nearest_distance = distance
			nearest = enemy
	nearest_enemy = nearest
