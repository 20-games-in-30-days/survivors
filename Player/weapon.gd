class_name Weapon
extends Node2D

static var filled_slots = 0
@export var is_default_weapon := false
@export_flags("Front", "Twin", "Shoulder", "Back", "Turret", "Bay") var weapon_slot := 0
@export var bullet_prefab: PackedScene
@export var player_path := ^".."
@export var bullet_damage := 1
@export var weapon_active := false:
	get:
		return weapon_active
	set(value):
		weapon_active = value
		visible = value
		if value:
			weapon_activated.emit(self)
			filled_slots |= weapon_slot
			if upgrade_logger:
				upgrade_logger.verbose("Weapon added. Weapon slots filled: b" + String.num_int64(filled_slots, 2))
		else:
			if filled_slots & weapon_slot:
				filled_slots &= ~weapon_slot
				if upgrade_logger:
					upgrade_logger.verbose("Weapon removed. Weapon slots filled: b" + String.num_int64(filled_slots, 2))

@export var weapon_level := 0:
	get:
		return weapon_level
	set(value):
		weapon_level = value
		_set_weapon_level(value)

var next_weapon_level:
	get:
		if weapon_active:
			return min(weapon_level + 1, max_weapon_level)
		else:
			return 0

var title:
	get:
		return _get_title()
		
var details:
	get:
		return _get_details()

var can_shoot:
	get:
		return !player.dead and weapon_active

var max_weapon_level := 0

var player: CharacterBody2D
var velocity: Vector2:
	get:
		return player.velocity
var logger: Logger
var upgrade_logger: Logger
signal weapon_activated(weapon)


func _ready():
	logger = Print.create_logger("Weapon %s" % name, Print.DEBUG, Print.VERBOSE)
	upgrade_logger = Print.get_logger("Upgrade Cards", true)
	player = get_node(player_path)
	weapon_active = is_default_weapon


## Upgrades this weapon to the next level.
func upgrade():
	if weapon_active:
		weapon_level = min(weapon_level + 1, max_weapon_level)
		logger.verbose("Upgraded %s to level %s" % [name, weapon_level])
	else:
		weapon_active = true
		logger.verbose("Activated Laser Turret")


func upgradable() -> bool:
	if !weapon_active:
		if filled_slots & weapon_slot:
			# We are not active, and the slot is full.
			logger.verbose("Weapon slot not available, slots are %s" % filled_slots)
			return false
		# We are not active, and the slot is open.
		return true
	else:
		# Do we have a remaining upgrade?
		return weapon_level < max_weapon_level


## Setter for the weapon level. MUST BE OVERRIDDEN IN THE CHILD CLASS. Use this to change values or appearance.
func _set_weapon_level(_value: int):
	logger.error("Set Weapon Level has no effect! Override this function in the weapon script!")


## Returns the title to display in an upgrade card. MUST BE OVERRIDDEN.
func _get_title() -> String:
	upgrade_logger.error("Weapon Script forgot to upgrade _get_title! Will return garbage.")
	return "A Weapon"


## Returns the label to display in an upgrade card. MUST BE OVERRIDDEN.
func _get_details() -> String:
	upgrade_logger.error("Weapon Script forgot to upgrade _get_label! Will return garbage.")
	return "Someone forgot to set the description!"

## Stops firing when the game ends.
func stop():
	weapon_active = false


## Resets this weapon.
func reset():
	weapon_active = is_default_weapon
	weapon_level = 0


# Get the forward vector.
func forward() -> Vector2:
	return global_transform.x.normalized()


## Creates a new bullet. The bullet does not exist in the scene until register_bullet is called.
## The bullet's starting velocity will match the player's velocity.
func new_bullet():
	var bullet = bullet_prefab.instantiate()
	bullet.linear_velocity = velocity
	bullet.damage = bullet_damage
	return bullet


## Add the bullet to the scene. Call this once you are done setting velocity, etc.
func register_bullet(bullet):
	Global.player_bullets.add_child(bullet)
