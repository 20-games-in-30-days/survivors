extends CharacterBody2D

@export var speed := 250
@export var rotation_speed := 2
@export var friction := 0.25
@onready var thruster = $Thruster/Anim
@onready var camera = $Camera2D
var thrusting := false
var dead := false

signal died
signal collected_mineral


func _physics_process(delta):
	if dead:
		return
	
	# Rotate
	var rot = Input.get_axis("left", "right")
	var add_velocity = Vector2.ZERO
	
	# Thrust
	if Input.is_action_pressed("thrust") and Global.game_state == Globals.GameState.PLAYING:
		add_velocity = transform.x.normalized() * speed * delta
		if !thrusting:
			thrusting = true
			thruster.play("Thrust")
	elif thrusting:
		thrusting = false
		thruster.play("RESET")
	
	rotation += rot * delta * rotation_speed
	
	# Friction
	velocity = (velocity * (1 - (friction * delta))) + add_velocity
	
	screen_wrap()
	move_and_slide()
	# Post to global so everyone can know where I am. This is just easier than asking Global to know where to get this.
	Global.player_pos = position
	Global.screen_center_pos = camera.get_screen_center_position()


func screen_wrap():
	if global_position.x < -Global.screen_wrap_size.x:
		global_position.x = Global.screen_wrap_size.x
	elif global_position.x > Global.screen_wrap_size.x:
		global_position.x = -Global.screen_wrap_size.x
	if global_position.y < -Global.screen_wrap_size.y:
		global_position.y = Global.screen_wrap_size.y
	elif global_position.y > Global.screen_wrap_size.y:
		global_position.y = -Global.screen_wrap_size.y


func _on_mineral_detect_body_entered(body):
	collected_mineral.emit()
	body.queue_free()


func _on_damage_zone_body_entered(_body):
	if !dead:
		visible = false
		thruster.play("RESET")
		velocity = Vector2.ZERO
		dead = true
		died.emit()


func regen():
	visible = true
	dead = false


func reset():
	global_position = Vector2.ZERO
	global_rotation_degrees = 90
	visible = true
	dead = false
