extends Camera2D

var menu_zoom
var tween: Tween


func _ready():
	var wrap_size = Global.screen_wrap_size + Global.overshoot_margin_pixels
	limit_bottom = wrap_size.y
	limit_top = -wrap_size.y
	limit_right = wrap_size.x
	limit_left = -wrap_size.x
	menu_zoom = zoom


func _process(_delta):
	var wrap_size = Global.screen_wrap_size + Global.overshoot_margin_pixels
	limit_bottom = wrap_size.y
	limit_top = -wrap_size.y
	limit_right = wrap_size.x
	limit_left = -wrap_size.x


func start():
	# Create a new Tween and add the zoom interpolation
	drag_horizontal_enabled = true
	drag_vertical_enabled = true
	tween = get_tree().create_tween()
	tween.tween_property(self, "zoom", Vector2.ONE, Global.menu_fade_time).set_trans(Tween.TRANS_QUAD).set_ease(Tween.EASE_IN_OUT)


func reset():
	drag_horizontal_enabled = false
	drag_vertical_enabled = false
	tween = get_tree().create_tween()
	tween.tween_property(self, "zoom", menu_zoom, Global.menu_fade_time).set_trans(Tween.TRANS_QUAD).set_ease(Tween.EASE_IN_OUT)
