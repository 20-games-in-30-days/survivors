extends Weapon

## The speed of the projectile.
@export var bullet_speed := 500
## How far the bullet travels before self-destructing.
@export var bullet_timeout := 5.0
## How long between shots (per barrel, is double the actual firing rate)
var reload_time := 2.0

# Flip-flop bit. Used to switch the side that we fire on.
var next_side = true
# Where are we in the firing sequence?
var cascade_step_l = 0
var cascade_step_r = 0

@onready var guns_left: Array[Node2D] = [$A, $C, $E]
@onready var guns_right: Array[Node2D] = [$B, $D, $F]
@onready var cooldown = $Cooldown
@onready var l_timer = $Cascade_L
@onready var r_timer = $Cascade_R
@onready var shoot_audio = $ShootAudio
@export var upgrades: Array[LaserRifleUpgrade]


func _ready():
	super._ready()
	cooldown.start(reload_time / 2.0)
	max_weapon_level = upgrades.size() - 1
	_set_weapon_level(0)


func fire(gun: Node2D):
	if gun.visible:
		var bullet: Bullet = new_bullet()
		bullet.global_position = gun.global_position
		bullet.linear_velocity += forward() * bullet_speed + Vector2(randf_range(-25, 25), randf_range(-25, 25))
		bullet.destroy_time = bullet_timeout
		register_bullet(bullet)
		shoot_audio.play()


## Returns the title for this weapon.
func _get_title():
	return upgrades[next_weapon_level].label


## Returns the label to display in an upgrade card. MUST BE OVERRIDDEN.
func _get_details() -> String:
	return upgrades[next_weapon_level].hint


func _set_weapon_level(level):
	var u: LaserRifleUpgrade = upgrades[level]
	$A.visible = u.barrel_a
	$B.visible = u.barrel_b
	$C.visible = u.barrel_c
	$D.visible = u.barrel_d
	$E.visible = u.barrel_e
	$F.visible = u.barrel_f
	reload_time = u.reload_time
	bullet_damage = u.bullet_damage
	if cooldown:
		cooldown.start(reload_time / 2.0)
		## Trigger a shot with our fancy new guns.
		_on_cooldown_timeout.call_deferred()


func _on_cooldown_timeout():
	if !can_shoot:
		return
	
	next_side = !next_side
	if next_side:
		fire(guns_left[0])
		cascade_step_l = 1
		l_timer.start()
	else:
		fire(guns_right[0])
		cascade_step_r = 1
		r_timer.start()


func _on_cascade_l_timeout():
	fire(guns_left[cascade_step_l])
	cascade_step_l += 1
	if cascade_step_l < guns_left.size():
		l_timer.start()


func _on_cascade_r_timeout():
	fire(guns_right[cascade_step_r])
	cascade_step_r += 1
	if cascade_step_r < guns_right.size():
		r_timer.start()
