class_name Railgun extends Weapon

@onready var cooldown = $Cooldown
@onready var shoot_audio = $ShootAudio
@export var upgrades: Array[RailgunUpgrade]
@export var damage := 2048
@export var damage_per_shot := 2048
@onready var animation_player = $AnimationPlayer

## Firing Sequence.
enum {
	CHARGING,
	FIRING,
	COOLDOWN
}
var cooldown_time := 5.0
var fire_anim = "fire1"


func _ready():
	super._ready()
	max_weapon_level = upgrades.size() - 1
	_set_weapon_level(0)


## Returns the title for this weapon.
func _get_title():
	return upgrades[next_weapon_level].label


## Returns the label to display in an upgrade card. MUST BE OVERRIDDEN.
func _get_details() -> String:
	return upgrades[next_weapon_level].hint


func _set_weapon_level(level):
	var u: RailgunUpgrade = upgrades[level]
	cooldown_time = u.cooldown_time
	damage_per_shot = u.bullet_damage
	fire_anim = u.fire_animation
	$Accent1.visible = u.accent1
	$Accent2.visible = u.accent2
	$Accent3.visible = u.accent3
	if cooldown:
		cooldown.start(cooldown_time)
		## Trigger a shot with our fancy new guns.
		_on_cooldown_timeout.call_deferred()


## The railgun has a fixed amount of damage to impart before it is out of energy.
func hit(damage_done):
	damage = max(0, damage - max(damage_done, damage * 0.1))


func stop():
	super.stop()
	animation_player.play("RESET")
	cooldown.start(0.5)


func _on_cooldown_timeout():
	if can_shoot:
		animation_player.play("charge")
	else:
		cooldown.start(0.5)


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "charge":
		damage = damage_per_shot
		animation_player.play(fire_anim)
	else:
		cooldown.start(cooldown_time)
