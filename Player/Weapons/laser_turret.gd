extends Weapon

## The speed of the projectile.
@export var bullet_speed := 500
## How far the bullet travels before self-destructing.
@export var bullet_timeout := 5.0

@onready var radar = $"../Radar"
@onready var guns = [$Barrel1, $Barrel2, $Barrel3, $Barrel4, $Barrel5]
@onready var cooldown = $Cooldown
@onready var shoot_audio = $ShootAudio
@export var upgrades: Array[LaserTurretUpgrade]
var active_guns = []
var rotation_speed = 0


func upgrade():
	super.upgrade()
	_set_weapon_level(weapon_level)


func _ready():
	super._ready()
	max_weapon_level = upgrades.size() - 1
	_set_weapon_level(0)


func _process(delta):
	var nearest_enemy = radar.nearest_enemy
	if weapon_active:
		if nearest_enemy:
			var direction_to_enemy = (nearest_enemy.global_position - global_position).normalized()
			var angle_to_enemy = atan2(direction_to_enemy.y, direction_to_enemy.x)
			global_rotation = lerp_angle(global_rotation, angle_to_enemy, rotation_speed * delta)
		else:
			global_rotation += 0.1 * rotation_speed * delta


func fire(gun: Node2D):
	if gun.visible:
		var bullet: Bullet = new_bullet()
		bullet.global_position = gun.global_position
		bullet.linear_velocity += forward() * bullet_speed
		bullet.destroy_time = bullet_timeout
		register_bullet(bullet)
		shoot_audio.play()


## Returns the title for this weapon.
func _get_title():
	return upgrades[next_weapon_level].label


## Returns the label to display in an upgrade card. MUST BE OVERRIDDEN.
func _get_details() -> String:
	return upgrades[next_weapon_level].hint


func _set_weapon_level(level):
	var u: LaserTurretUpgrade = upgrades[level]
	$Head.visible = weapon_active
	$Barrel1.visible = weapon_active and u.barrel_a
	$Barrel2.visible = weapon_active and u.barrel_b
	$Barrel3.visible = weapon_active and u.barrel_c
	$Barrel4.visible = weapon_active and u.barrel_d
	$Barrel5.visible = weapon_active and u.barrel_e
	if weapon_active:
		active_guns = []
		for barrel in guns:
			if barrel.visible:
				active_guns.append(barrel)
		rotation_speed = u.rotation_speed
		bullet_damage = u.bullet_damage
		cooldown.start(u.reload_time)
	else:
		cooldown.stop()


func _on_cooldown_timeout():
	if can_shoot:
		var g = active_guns.pop_front()
		fire(g)
		active_guns.push_back(g)
