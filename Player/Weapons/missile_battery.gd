extends Weapon

## The speed of the projectile.
@export var bullet_speed := 500
## How far the bullet travels before self-destructing.
@export var bullet_timeout := 5.0
## How long between shots (per barrel, is double the actual firing rate)
var reload_time := 2.0
var cascade_time := 1.0
var shots_per_cascade := 4
var shot_count := 0

@onready var guns: Array[Node2D] = [$A1, $B1, $A2, $B2, $A3, $B3, $A4, $B4]
@onready var radar = $"../Radar"
@onready var cooldown = $Cooldown
@onready var cascade = $Cascade
@onready var shoot_audio = $ShootAudio
@export var upgrades: Array[MissileBatteryUpgrade]

func _ready():
	super._ready()
	max_weapon_level = upgrades.size() - 1
	_set_weapon_level(0)


func fire(gun: Node2D):
	if gun.visible and radar.enemy_detected:
		var bullet: Missile = new_bullet()
		bullet.radar = radar
		bullet.global_position = gun.global_position
		bullet.global_rotation = global_rotation
		bullet.linear_velocity += forward() * bullet_speed
		bullet.destroy_time = bullet_timeout
		register_bullet(bullet)
		shoot_audio.play()


## Returns the title for this weapon.
func _get_title():
	return upgrades[next_weapon_level].label


## Returns the label to display in an upgrade card. MUST BE OVERRIDDEN.
func _get_details() -> String:
	return upgrades[next_weapon_level].hint


func _set_weapon_level(level):
	var u: MissileBatteryUpgrade = upgrades[level]
	reload_time = u.cooldown_time
	bullet_damage = u.bullet_damage
	cascade_time = u.fire_delay
	shots_per_cascade = u.shots_per_cascade
	## Trigger a shot with our fancy new guns.
	_on_cooldown_timeout.call_deferred()


func _on_cooldown_timeout():
	if can_shoot:
		shot_count = 0
		cascade.start(cascade_time)
	else:
		cooldown.start(0.5)


func _on_cascade_timeout():
	if can_shoot:
		var g = guns.pop_front()
		fire(g)
		guns.push_back(g)
		shot_count += 1
		if shot_count < shots_per_cascade:
			cascade.start(cascade_time)
		else:
			cooldown.start(reload_time)
	else:
		cooldown.start(0.5)
