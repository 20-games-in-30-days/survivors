extends Resource
class_name SpawnCount

@export_group("Minimum Counts")
@export var asteroid_small_min : int = 0
@export var asteroid_0_min : int = 0
@export var asteroid_1_min : int = 0
@export var asteroid_2_min : int = 0
@export var asteroid_3_min : int = 0
@export var asteroid_4_min : int = 0
@export var asteroid_5_min : int = 0
@export var asteroid_6_min : int = 0
@export var asteroid_7_min : int = 0
@export var asteroid_8_min : int = 0
@export var asteroid_9_min : int = 0

@export_group("Impulse Counts")
@export var asteroid_small_impulse : int = 0
@export var asteroid_0_impulse : int = 0
@export var asteroid_1_impulse : int = 0
@export var asteroid_2_impulse : int = 0
@export var asteroid_3_impulse : int = 0
@export var asteroid_4_impulse : int = 0
@export var asteroid_5_impulse : int = 0
@export var asteroid_6_impulse : int = 0
@export var asteroid_7_impulse : int = 0
@export var asteroid_8_impulse : int = 0
@export var asteroid_9_impulse : int = 0

var logger: Logger


func _init():
	logger = Print.get_logger("WaveManager", true)


## Return the minimum number of enemies to continually spawn for this wave.
func get_minimum_count(enemy_type: Globals.AsteroidSize) -> int:
	match enemy_type:
		Globals.AsteroidSize.ASTEROID_SMALL: return asteroid_small_min
		Globals.AsteroidSize.ASTEROID_0: return asteroid_0_min
		Globals.AsteroidSize.ASTEROID_1: return asteroid_1_min
		Globals.AsteroidSize.ASTEROID_2: return asteroid_2_min
		Globals.AsteroidSize.ASTEROID_3: return asteroid_3_min
		Globals.AsteroidSize.ASTEROID_4: return asteroid_4_min
		Globals.AsteroidSize.ASTEROID_5: return asteroid_5_min
		Globals.AsteroidSize.ASTEROID_6: return asteroid_6_min
		Globals.AsteroidSize.ASTEROID_7: return asteroid_7_min
		Globals.AsteroidSize.ASTEROID_8: return asteroid_8_min
		Globals.AsteroidSize.ASTEROID_9: return asteroid_9_min
		_:
			logger.error("Asked spawn_wave for unknown enemy type: %s" % enemy_type)
			return Globals.AsteroidSize.MAX_ASTEROIDS


func get_impulse_count(enemy_type: Globals.AsteroidSize) -> int:
	match enemy_type:
		Globals.AsteroidSize.ASTEROID_SMALL: return asteroid_small_impulse
		Globals.AsteroidSize.ASTEROID_0: return asteroid_0_impulse
		Globals.AsteroidSize.ASTEROID_1: return asteroid_1_impulse
		Globals.AsteroidSize.ASTEROID_2: return asteroid_2_impulse
		Globals.AsteroidSize.ASTEROID_3: return asteroid_3_impulse
		Globals.AsteroidSize.ASTEROID_4: return asteroid_4_impulse
		Globals.AsteroidSize.ASTEROID_5: return asteroid_5_impulse
		Globals.AsteroidSize.ASTEROID_6: return asteroid_6_impulse
		Globals.AsteroidSize.ASTEROID_7: return asteroid_7_impulse
		Globals.AsteroidSize.ASTEROID_8: return asteroid_8_impulse
		Globals.AsteroidSize.ASTEROID_9: return asteroid_9_impulse
		_:
			logger.error("Asked spawn_wave for unknown enemy type: %s" % enemy_type)
			return Globals.AsteroidSize.MAX_ASTEROIDS


func impulse_string():
	var details := "SpawnWave Details:\n"
	details += "Impulse Counts:\n"
	details += "\tAsteroid Small Impulse: %s\n" % asteroid_small_impulse
	details += "\tAsteroid 0 Impulse: %s\n" % asteroid_0_impulse
	details += "\tAsteroid 1 Impulse: %s\n" % asteroid_1_impulse
	details += "\tAsteroid 2 Impulse: %s\n" % asteroid_2_impulse
	details += "\tAsteroid 3 Impulse: %s\n" % asteroid_3_impulse
	details += "\tAsteroid 4 Impulse: %s\n" % asteroid_4_impulse
	details += "\tAsteroid 5 Impulse: %s\n" % asteroid_5_impulse
	details += "\tAsteroid 6 Impulse: %s\n" % asteroid_6_impulse
	details += "\tAsteroid 7 Impulse: %s\n" % asteroid_7_impulse
	details += "\tAsteroid 8 Impulse: %s\n" % asteroid_8_impulse
	details += "\tAsteroid 9 Impulse: %s\n" % asteroid_9_impulse
	return details


func _to_string() -> String:
	var details := "SpawnWave Details:\n"
	details += "Minimum Counts:\n"
	details += "\tAsteroid Small Min: %s\n" % asteroid_small_min
	details += "\tAsteroid 0 Min: %s\n" % asteroid_0_min
	details += "\tAsteroid 1 Min: %s\n" % asteroid_1_min
	details += "\tAsteroid 2 Min: %s\n" % asteroid_2_min
	details += "\tAsteroid 3 Min: %s\n" % asteroid_3_min
	details += "\tAsteroid 4 Min: %s\n" % asteroid_4_min
	details += "\tAsteroid 5 Min: %s\n" % asteroid_5_min
	details += "\tAsteroid 6 Min: %s\n" % asteroid_6_min
	details += "\tAsteroid 7 Min: %s\n" % asteroid_7_min
	details += "\tAsteroid 8 Min: %s\n" % asteroid_8_min
	details += "\tAsteroid 9 Min: %s\n" % asteroid_9_min
	return details
