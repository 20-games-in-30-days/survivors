extends Node2D

const MINERAL = preload("res://Game/mineral.tscn")
const EXPLOSION_SMALL = preload("res://Particles/explosion_small.tscn")

@export var AsteroidPrefabs: Array[PackedScene]
@export var spawn_counts: SpawnCount
@export var max_asteroids := 100
@onready var minerals = $"../Minerals"
@onready var particles = $"../Particles"

var current_enemy_count: Array[int]

var start_color: Color


func _spawn_impulse():
	for i in range(len(AsteroidPrefabs)):
			for j in spawn_counts.get_impulse_count(i):
				spawn_asteroid(i)


func _ready():
	current_enemy_count.clear()
	current_enemy_count.resize(AsteroidPrefabs.size())
	Global.asteroids = self
	start_color = modulate


func _process(_delta):
	# Spawn missing objects:
	if Global.playing():
		for i in range(len(AsteroidPrefabs)):
			while current_enemy_count[i] < spawn_counts.get_minimum_count(i):
				spawn_asteroid(i)


func spawn_asteroid(asteroid_type: int):
	var spawn_pos = Global.get_random_edge_point()
	var asteroid_instance: Asteroid = AsteroidPrefabs[asteroid_type].instantiate()
	@warning_ignore("int_as_enum_without_cast")
	asteroid_instance.type = asteroid_type
	asteroid_instance.position = spawn_pos
	add_child(asteroid_instance)
	current_enemy_count[asteroid_type] += 1
	asteroid_instance.connect("destroyed", _on_asteroid_destroyed)


func _on_asteroid_destroyed(t: Globals.AsteroidSize, asteroid: RigidBody2D):
	current_enemy_count[t] -= 1
	var mineral = MINERAL.instantiate()
	mineral.global_position = asteroid.global_position
	mineral.rotate(randi())
	minerals.add_child.call_deferred(mineral)
	
	var explosion = EXPLOSION_SMALL.instantiate()
	explosion.global_position = asteroid.global_position
	particles.add_child.call_deferred(explosion)
	
	# Only break the asteroids if the game is still going.
	if Global.game_state != Globals.GameState.PLAYING:
		return
	
	# Spawn smaller asteroids when this one breaks.
	if t > Globals.AsteroidSize.ASTEROID_SMALL and current_enemy_count[t - 1] < max_asteroids:
		spawn_at_pos(asteroid.position, asteroid.linear_velocity, t-1)
		if t < Globals.AsteroidSize.ASTEROID_5:
			spawn_at_pos(asteroid.position, asteroid.linear_velocity, t-1)
		else:
			spawn_at_pos(asteroid.position, asteroid.linear_velocity, randi_range(Globals.AsteroidSize.ASTEROID_1, Globals.AsteroidSize.ASTEROID_4))


func spawn_at_pos(pos, vel, t):
	var asteroid_instance: Asteroid = AsteroidPrefabs[t].instantiate()
	@warning_ignore("int_as_enum_without_cast")
	asteroid_instance.type = t
	asteroid_instance.position = pos
	asteroid_instance.inherited_linear_velocity = vel
	add_child.call_deferred(asteroid_instance)
	current_enemy_count[t] += 1
	asteroid_instance.connect("destroyed", _on_asteroid_destroyed)


func reset():
	for i in current_enemy_count.size():
		current_enemy_count[i] = 0
	var tween = get_tree().create_tween()
	tween.tween_property(self, "modulate", Color.TRANSPARENT, Global.menu_fade_time)
	await get_tree().create_timer(Global.menu_fade_time, false).timeout
	for child in get_children():
		child.queue_free()
	modulate = start_color
