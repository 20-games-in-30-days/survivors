class_name Asteroid extends WrappedBody

## All of the possible shapes. Will be scaled up for larger asteroids.
@export var type: Globals.AsteroidSize = Global.AsteroidSize.ASTEROID_SMALL
@export var health := 1

## The starting speed of the asteroid. (Min, Max)
@export var inherited_linear_velocity := Vector2.ZERO
@export var start_speed_range := Vector2(100, 200)
@export var angular_velocity_range := Vector2(-90, 90)

@onready var shape = $Shape

signal destroyed(t: Globals.AsteroidSize, me)

## Skip this if we aren't actually playing.
func _ready():
	if Global.game_state == Globals.GameState.PLAYING:
		var points = shape.shapes.pick_random()
		var scaled_points = []
		shape.points.clear()
		for p in points:
			scaled_points.append(p * (type + 1))
		shape.points = scaled_points
		$Collider.shape.radius = 16 * (type + 1)
		if type < Globals.AsteroidSize.ASTEROID_5:
			$Area2D/Collider.shape.radius = 16 * (type + 1)
		else:
			$Area2D/Collider.polygon = shape.points
		linear_velocity += Vector2(randf() - 0.5, randf() - 0.5).normalized() * randf_range(start_speed_range.x, start_speed_range.y)
		angular_velocity += deg_to_rad(randf_range(angular_velocity_range.x, angular_velocity_range.y))
		@warning_ignore("narrowing_conversion")
		health = pow(2, type * 2)


## Destroy when we leave the screen to slowly reduce the number of objects.
func reset():
	wrap_type = wrapType.DESTROY


func _on_bullet_entered(body):
	var damage_done = health
	if body is Bullet or body is Missile or body is Railgun:
		health -= body.damage
		if health > 0:
			damage_done = body.damage
	else:
		health = 0
	body.hit(damage_done)
	if health <= 0:
		emit_signal("destroyed", type, self)
		queue_free()
