extends Control

@export var available_colors: Array[Color]


func start():
	modulate = available_colors.pick_random()
