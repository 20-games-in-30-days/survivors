class_name WrappedBody
extends RigidBody2D

## A wrapped body can either wrap around the screen, or destroy itself upon leaving the screen edge.
enum wrapType {
	WRAP,
	DESTROY
}
var wrap_type := wrapType.WRAP


func _physics_process(_delta):
	## Wrap if we are outside the screen. Skip if we are in the menu (special case)
	if !Global.screen_wrap_rect.has_point(global_position) and Global.game_state == Globals.GameState.PLAYING:
		if wrap_type == wrapType.DESTROY:
			queue_free()
		else:
			position = Vector2(
				wrap(position.x, Global.screen_wrap_rect.position.x, Global.screen_wrap_rect.position.x + Global.screen_wrap_rect.size.x),
				wrap(position.y, Global.screen_wrap_rect.position.y, Global.screen_wrap_rect.position.y + Global.screen_wrap_rect.size.y))
