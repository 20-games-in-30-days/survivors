class_name UpgradeCard extends Node

signal buff_activated

var max_upgrade_level := 0
var upgrade_level := 0
var logger: Logger
@export var upgrade_active := false:
	get:
		return upgrade_active
	set(value):
		upgrade_active = value
		if value:
			buff_activated.emit(self)


var next_upgrade_level:
	get:
		if upgrade_active:
			return min(upgrade_level + 1, max_upgrade_level)
		else:
			return 0

var title:
	get:
		return _get_title()
		
var details:
	get:
		return _get_details()


func _ready():
	logger = Print.get_logger("Upgrade Cards", true)
	add_to_group("resettable")


func stop():
	upgrade_active = false
	upgrade_level = 0


func upgrade():
	if !upgrade_active:
		upgrade_active = true
	else:
		upgrade_level = min(upgrade_level + 1, max_upgrade_level)


func upgradable():
	return upgrade_level < max_upgrade_level


## Returns the title to display in an upgrade card. MUST BE OVERRIDDEN.
func _get_title() -> String:
	logger.error("Upgrade Script forgot to upgrade _get_title! Will return garbage.")
	return "An Upgrade"


## Returns the label to display in an upgrade card. MUST BE OVERRIDDEN.
func _get_details() -> String:
	logger.error("Upgrade Script forgot to upgrade _get_label! Will return garbage.")
	return "Someone forgot to set the description!"
