extends Label

@onready var spawn_timer = $"../../../../GameViewport/Game/SpawnTimer"
@onready var level_progress = $"../../../LevelProgress"


func _process(_delta):
	if spawn_timer.is_playing() and spawn_timer.current_animation == "Level1":
		# Calculate time in seconds
		var time_in_seconds = spawn_timer.current_animation_position / spawn_timer.speed_scale
		# Calculate minutes and seconds
		var minutes = int(time_in_seconds / 60.0)
		var seconds = int(time_in_seconds) % 60
		# Format time as MM:SS
		text = "Time: %02d:%02d\nLevel %s" % [minutes, seconds, level_progress.current_level]
	else:
		text = "Time: 00:00\nLevel 0"
