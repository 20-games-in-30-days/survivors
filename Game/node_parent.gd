extends Node2D

var start_color: Color


func _ready():
	start_color = modulate


func reset():
	var tween = get_tree().create_tween()
	tween.tween_property(self, "modulate", Color.TRANSPARENT, Global.menu_fade_time)
	await get_tree().create_timer(Global.menu_fade_time, false).timeout
	for child in get_children():
		child.queue_free()
	modulate = start_color
