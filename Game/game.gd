extends Node2D

const PLAYER_DIE = preload("res://Particles/player_die.tscn")

@export_range(0, 1, 0.1) var screen_wrap_progress := 0.0:
	get:
		return screen_wrap_progress
	set(value):
		screen_wrap_progress = value
		Global.set_wrap(value)

@onready var screen_wrap_rect = $UI/screen_wrap_rect
@onready var screen_wrap_size = $UI/screen_wrap_size
@onready var spawn_timer = $SpawnTimer
@onready var main_menu = $UI/MainMenu
@onready var start_asteroid = $UI/MainMenu/StartAsteroid
@onready var player = $Player
@onready var player_bullets = $PlayerBullets
@onready var enemy_bullets = $EnemyBullets
@onready var enemies = $Enemies
@onready var minerals = $Minerals
@onready var particles = $Particles
@onready var upgrades_menu = $"../../Modulate/MC/UI/UpgradesMenu"
@onready var life_counter = $"../../Modulate/MC/UI/LifeCounter"
@onready var start_asteroid_break = $UI/MainMenu/StartAsteroid/Break
@onready var spawn_counts = $"../../Modulate/MC/UI/SpawnCounts"


var menu_color: Color


func _ready():
	Global.player_bullets = player_bullets
	Global.enemy_bullets = enemy_bullets


## Start the gameplay loop.
func start():
	spawn_timer.play("Level1")
	Global.game_state = Globals.GameState.PLAYING
	menu_color = main_menu.modulate
	var tween = get_tree().create_tween()
	tween.tween_property(main_menu, "modulate", Color.TRANSPARENT, Global.menu_fade_time)


## Go back to the menu.
func reset():
	spawn_timer.play("RESET")
	start_asteroid.visible = true
	var tween = get_tree().create_tween()
	tween.tween_property(main_menu, "modulate", menu_color, Global.menu_fade_time)
	await get_tree().create_timer(Global.menu_fade_time, false).timeout


func _process(_delta):
	# Debugging visualizers.
	screen_wrap_rect.set_begin(Global.screen_wrap_rect.position)
	screen_wrap_rect.set_size(Global.screen_wrap_rect.size)
	screen_wrap_size.set_size(Global.screen_wrap_size * 2)
	screen_wrap_size.set_position(-Global.screen_wrap_size)
	if enemies.spawn_counts:
		spawn_counts.text = str(enemies.spawn_counts)


func _on_player_died():
	var explosion = PLAYER_DIE.instantiate()
	explosion.global_position = player.global_position
	add_child.call_deferred(explosion)
	if life_counter.life_count > 0:
		Global.game_state = Globals.GameState.PLAYER_DEAD
		await get_tree().create_timer(3, false).timeout
		life_counter.lose_life()
		await get_tree().create_timer(1, false).timeout
		player.regen()
		await get_tree().create_timer(1, false).timeout
		Global.game_state = Globals.GameState.PLAYING
	else:
		Global.game_over()


func _on_start_asteroid_hit(body):
	if Global.game_state == Globals.GameState.MAIN_MENU:
		start_asteroid.visible = false
		body.queue_free()
		get_tree().call_group("gameStart", "start")
		start_asteroid_break.play()


func _on_level_up():
	life_counter.regen_life()
	upgrades_menu.open_menu()
