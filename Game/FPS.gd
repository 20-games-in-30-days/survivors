extends Label


func _process(_delta):
	text = "FPS: " + str(Engine.get_frames_per_second())
	if Input.is_action_just_pressed("toggle_fps"):
		visible = !visible
