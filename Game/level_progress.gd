extends TextureProgressBar

## What value does the level up curve go to?
@export_range(25, 500, 5) var max_intended_levels := 70.0
@export var levels_per_loss := 20
@export var level_up_curve: Curve
@export var animate_time := 0.25
@export var debugging_level := false
@export var max_level := 55
var mineral_count := 0
var current_level := 0
var minerals_to_level_up := 0.0
var logger: Logger
var reset_level := 0
var tween: Tween

signal level_up


func _ready():
	logger = Print.create_logger("Level Up", Print.DEBUG, Print.DEBUG)
	reset_level = max_intended_levels
	reset()


func reset():
	if current_level > 30:
		max_intended_levels = reset_level
		logger.debug("Max level reset to %s" % reset_level)
	else:
		max_intended_levels += levels_per_loss
		logger.debug("Max level increased to %s" % max_intended_levels)
	current_level = 0
	mineral_count = 0
	minerals_to_level_up = level_up_curve.sample(current_level / max_intended_levels)
	max_value = minerals_to_level_up * 100.0
	logger.info("Minerals to next level up: " + str(minerals_to_level_up))
	if tween:
		tween.kill()
	tween = get_tree().create_tween().set_pause_mode(Tween.TWEEN_PAUSE_PROCESS)
	tween.tween_property(self, "value", 0, Global.popup_menu_fade_time)


func next_level():
	current_level += 1
	mineral_count = 0
	minerals_to_level_up = ceil(level_up_curve.sample(current_level / max_intended_levels))
	# Square it so we don't have to keep dealing with level ups.
	if current_level > max_level:
		minerals_to_level_up *= minerals_to_level_up
	max_value = minerals_to_level_up * 100.0
	if tween:
		tween.kill()
	tween = get_tree().create_tween().set_pause_mode(Tween.TWEEN_PAUSE_PROCESS)
	tween.tween_property(self, "value", 0, Global.popup_menu_fade_time)
	logger.info("Level up! Minerals to next level up: %s" % minerals_to_level_up)


func _on_player_collected_mineral():
	if Global.playing():
		if debugging_level:
			level_up.emit()
			return
		mineral_count += 1
		logger.verbose("Mineral Added, total %s of %s" % [mineral_count, minerals_to_level_up])
		if tween:
			tween.kill()
		tween = get_tree().create_tween().set_pause_mode(Tween.TWEEN_PAUSE_PROCESS)
		tween.tween_property(self, "value", mineral_count * 100.0, animate_time)
		if mineral_count >= minerals_to_level_up:
			mineral_count = 0
			level_up.emit()


func _on_upgrades_menu_upgrade_selected():
	next_level()
