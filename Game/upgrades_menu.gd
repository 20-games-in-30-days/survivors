extends VBoxContainer
@onready var button1 = $Options/Margin1/Button
@onready var button2 = $Options/Margin2/Button
@onready var button3 = $Options/Margin3/Button
@onready var titles = [$Options/Margin1/VBoxContainer/Title, $Options/Margin2/VBoxContainer/Title, $Options/Margin3/VBoxContainer/Title]
@onready var details = [$Options/Margin1/VBoxContainer/Details, $Options/Margin2/VBoxContainer/Details, $Options/Margin3/VBoxContainer/Details]

var all_cards: Array[Node]
var all_weapons: Array[Weapon] = []
var all_buffs: Array[Node] = []
var active_weapons := []
var active_buffs := []
var menu_cards := []
var logger: Logger

signal upgrade_selected(pos: int)


func _ready():
	logger = Print.create_logger("Upgrade Cards", Print.DEBUG, Print.VERBOSE)
	button1.process_mode = Node.PROCESS_MODE_WHEN_PAUSED
	button2.process_mode = Node.PROCESS_MODE_WHEN_PAUSED
	button3.process_mode = Node.PROCESS_MODE_WHEN_PAUSED
	
	all_cards = get_tree().get_nodes_in_group("upgradeCard")
	for card in all_cards:
		if card is Weapon:
			all_weapons.append(card)
			card.weapon_activated.connect(_on_weapon_activated)
			## Make sure that we don't miss this, the signal may have already been sent.
			if card.weapon_active:
				active_weapons.append(card)
		else:
			all_buffs.append(card)
			card.buff_activated.connect(_on_weapon_activated)


func update_cards():
	# Reset menu cards for new selection
	menu_cards.clear()
	logger.verbose("Selecting new upgrade cards.")
	
	# Filter out upgradable weapons and buffs
	var upgradable_weapons = all_weapons.filter(func(w): return w.upgradable())
	var upgradable_buffs = all_buffs.filter(func(b): return b.upgradable())

	# Combine filtered lists and shuffle
	var potential_upgrades = upgradable_weapons + upgradable_buffs
	potential_upgrades.shuffle()

	# Limit selection to 3, prioritizing weapons if they're not already maxed
	for i in range(3):
		if potential_upgrades.size() > 0:
			var card = potential_upgrades.pop_front()
			menu_cards.append(card)
			titles[i].text = card.title
			details[i].text = card.details
		else:
			menu_cards.append(self)
			titles[i].text = "No Upgrades Available"
			details[i].text = "Don't blame the game designer!"
	
	await get_tree().create_timer(0.75).timeout
	button2.call_deferred("grab_focus")


func open_menu():
	update_cards()
	var tween = get_tree().create_tween().set_pause_mode(Tween.TWEEN_PAUSE_PROCESS)
	get_tree().paused = true
	tween.tween_property(self, "modulate", Color.WHITE, Global.popup_menu_fade_time)


func close_menu():
	var tween = get_tree().create_tween().set_pause_mode(Tween.TWEEN_PAUSE_PROCESS)
	tween.finished.connect(_closed)
	tween.tween_property(self, "modulate", Color.TRANSPARENT, Global.popup_menu_fade_time)


## So we don't throw errors if there aren't enough cards.
func upgrade():
	logger.info("Player selected not to upgrade.")


func stop():
	active_weapons.clear()
	active_buffs.clear()


func _on_weapon_activated(weapon):
	active_weapons.append(weapon)


func _on_buff_activated(buff):
	active_buffs.append(buff)


func _closed():
	get_tree().paused = false


func _on_button1_pressed():
	menu_cards[0].upgrade()
	upgrade_selected.emit()
	close_menu()


func _on_button2_pressed():
	menu_cards[1].upgrade()
	upgrade_selected.emit()
	close_menu()


func _on_button3_pressed():
	menu_cards[2].upgrade()
	upgrade_selected.emit()
	close_menu()
