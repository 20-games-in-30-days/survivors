class_name Globals
extends Node2D

## Start and end wrap size for the level. End size is the maximum size of the background, etc.
@export var start_wrap_size := Vector2(2000, 1000)
@export var end_wrap_size := Vector2(5000, 10000)

## The time it takes to zoom out from the main menu at the start.
@export var menu_fade_time := 1.0
## The time it takes to enter or exit a popup or pause menu.
@export var popup_menu_fade_time := 0.25
## The time to dwell on a game over before returning to the menu.
@export var game_over_time := 3.0

## How far to overshoot the edge of the map in pixels. Used by objects and the player camera.
@export_range(0.001, 1) var overshoot_margin := 0.2


## Easy reference to add bullets and objects as children.
var player_bullets: Node2D
var enemy_bullets: Node2D
var asteroids: Node2D

## Variables for tracking the size and location of the game window.
var screen_wrap_size: Vector2
var overshoot_margin_pixels: Vector2
var screen_wrap_rect: Rect2
var player_pos: Vector2
var screen_center_pos: Vector2

## Screen wrap size changed.
signal wrap_size_changed()

## Asteroid or enemy types.
enum AsteroidSize {
	# Asteroids need to be at the end of the list, we compare with the >= operator.
	ASTEROID_SMALL,
	ASTEROID_0,
	ASTEROID_1,
	ASTEROID_2,
	ASTEROID_3,
	ASTEROID_4,
	ASTEROID_5,
	ASTEROID_6,
	ASTEROID_7,
	ASTEROID_8,
	ASTEROID_9,
	MAX_ASTEROIDS
}

## Controls spawning and other global things.
enum GameState {
	MAIN_MENU,
	PLAYING,
	PLAYER_DEAD,
	GAMEOVER
}

@export var game_state := GameState.MAIN_MENU


## Get all references to globals for easy access.
## Fallback: Log a warning, then use this node as a parent for bullets, etc.
func _ready():
	# Fallback. This should be overwritten.
	player_bullets = self
	enemy_bullets = self
	asteroids = self
	
	var s = ""
	for i in range(AsteroidSize.MAX_ASTEROIDS):
		s += "\nAsteroid type %s has %s health." % [i, pow(2, i * 2)]
	Print.info(s)
	
	set_wrap(0)
	screen_wrap_rect = Rect2(-screen_wrap_size.x, -screen_wrap_size.y, screen_wrap_size.x * 2, screen_wrap_size.y * 2)


func set_wrap(progress: float):
	# Set our screen wrap. Make sure that it isn't smaller than the actual screen (including overshoot).
	var desired_wrap = lerp(start_wrap_size, end_wrap_size, progress)
	
	screen_wrap_size = Vector2(
			max(desired_wrap.x, get_viewport_rect().size.x * (1 + overshoot_margin)),
			max(desired_wrap.y, get_viewport_rect().size.y * (1 + overshoot_margin)))
	# Convert the percent to pixels. 
	overshoot_margin_pixels = get_viewport_rect().size * overshoot_margin


func playing():
	return game_state == GameState.PLAYING


func start():
	game_state = GameState.PLAYING


func game_over():
	game_state = GameState.GAMEOVER
	get_tree().call_group("resettable", "stop")
	await get_tree().create_timer(game_over_time, false).timeout
	get_tree().call_group("resettable", "reset")
	game_state = GameState.MAIN_MENU


func reset():
	game_state = GameState.MAIN_MENU


## Get a point along the edge of the screen. Useful for spawning things.
func get_random_edge_point() -> Vector2:
	var edge = randi() % 4
	var x
	var y

	match edge:
		0: # Top edge
			x = randf_range(screen_wrap_rect.position.x, screen_wrap_rect.position.x + screen_wrap_rect.size.x)
			y = screen_wrap_rect.position.y
		1: # Bottom edge
			x = randf_range(screen_wrap_rect.position.x, screen_wrap_rect.position.x + screen_wrap_rect.size.x)
			y = screen_wrap_rect.position.y + screen_wrap_rect.size.y
		2: # Left edge
			x = screen_wrap_rect.position.x
			y = randf_range(screen_wrap_rect.position.y, screen_wrap_rect.position.y + screen_wrap_rect.size.y)
		3: # Right edge
			x = screen_wrap_rect.position.x + screen_wrap_rect.size.x
			y = randf_range(screen_wrap_rect.position.y, screen_wrap_rect.position.y + screen_wrap_rect.size.y)

	return Vector2(x, y)


func _process(_delta):
	screen_wrap_rect = Rect2(-screen_wrap_size.x, -screen_wrap_size.y, screen_wrap_size.x * 2, screen_wrap_size.y * 2)
	var offset = Vector2.ZERO
	if screen_center_pos.x < -overshoot_margin_pixels.x:
		offset.x = -overshoot_margin_pixels.x
	elif screen_center_pos.x > overshoot_margin_pixels.x:
		offset.x = overshoot_margin_pixels.x
	if screen_center_pos.y < -overshoot_margin_pixels.y:
		offset.y = -overshoot_margin_pixels.y
	elif screen_center_pos.y > overshoot_margin_pixels.y:
		offset.y = overshoot_margin_pixels.y
	screen_wrap_rect.position += offset
