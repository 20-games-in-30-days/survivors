extends Node

var game_pause := false


func _process(_delta):
	# Pause the game.
	if Input.is_action_just_pressed("pause"):
		if Global.game_state == Globals.GameState.MAIN_MENU:
			return
		if game_pause and get_tree().paused:
			get_tree().paused = false
			game_pause = false
		elif !game_pause and !get_tree().paused:
			get_tree().paused = true
			game_pause = true
		elif game_pause and !get_tree().paused:
			Print.error("The game isn't paused, but thinks it is. Could this be a race condition?")
			get_tree().paused = true
	
	if Input.is_action_just_pressed("toggle_music"):
		AudioServer.set_bus_mute(4, !AudioServer.is_bus_mute(4))
	if Input.is_action_just_pressed("toggle_noise"):
		AudioServer.set_bus_mute(0, !AudioServer.is_bus_mute(0))
