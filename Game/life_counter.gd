extends MarginContainer

@onready var lives = [$Lives/Life0, $Lives/Life1, $Lives/Life2, $Lives/Life3, $Lives/Life4, $Lives/Life5, $Lives/Life6, $Lives/Life7, $Lives/Life8, $Lives/Life9]

@export var available_color: Color
@export var used_color: Color

var unlocked_lives := 0
var life_count := 0
var regen_per_level := 0


func _ready():
	update_lives()


## Get rid of one of our lives.
func lose_life():
	life_count = max(life_count - 1, 0)
	update_lives()


## Add an unlocked life.
func unlock_life(l):
	unlocked_lives = min(l, lives.size() - 1)
	regen_all_lives()


## Regenerate all lost lives.
func regen_all_lives():
	life_count = unlocked_lives
	update_lives()


## Regenerate one or more lives, based on the regeneration level.
func regen_life():
	life_count = min(unlocked_lives, life_count + regen_per_level)
	update_lives()


## Re-draw the life counter.
func update_lives():
	for i in lives.size():
		var life_color := Color.TRANSPARENT
		if i < life_count:
			life_color = available_color
		elif i < unlocked_lives:
			life_color = used_color
		lives[i].modulate = life_color


## Get rid of all lives.
func reset():
	unlocked_lives = 0
	regen_all_lives()

